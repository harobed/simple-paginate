=======================
Simple Paginate project
=======================

Pagination
==========

::

    >>> from simple_paginate import Pagination
    >>> p = Pagination(total=45)
    >>> p
    Page :
        Current page : 1
        First item : 1
        Last item : 10
        First page : 1
        Last Page : 5
        Previous page : None
        Next page : 2
        Items per page : 10
        Total number of items : 45
        Total number of pages : 5
    >>> p.pages()
    [1, 2, 3, 4, 5]

    >>> p = Pagination(page=8, total=500)
    >>> p.pages()
    [1, 2, None, 6, 7, 8, 9, 10, None, 49, 50]
    >>> p.previous_page
    7
    >>> p.next_page
    9


Paginate collection
===================

Simple collection
-----------------

::

    >>> from simple_paginate import Paginate
    >>> c = ['item%s' % i for i in range(1, 120)]
    >>> p = Paginate(c, page=6, per_page=5)
    >>> p
    Page :
        Current page : 6
        First item : 26
        Last item : 30
        First page : 1
        Last Page : 24
        Previous page : 5
        Next page : 7
        Items per page : 5
        Total number of items : 119
        Total number of pages : 24
    >>> p.pages()
    [1, 2, None, 4, 5, 6, 7, 8, None, 23, 24]
    >>> [item for item in p.items]
    ['item26', 'item27', 'item28', 'item29', 'item30']
    >>> [item for item in p]
    ['item26', 'item27', 'item28', 'item29', 'item30']
    >>> p[2]
    'item28'


SQLAlchemy collection
---------------------

First, I create a dummy entity

::

    >>> import sqlalchemy as sa
    >>> from sqlalchemy.ext.declarative import declarative_base
    >>> from sqlalchemy.orm import sessionmaker
    >>> engine = sa.create_engine('sqlite:///:memory:')
    >>> Base = declarative_base()
    >>> Session = sessionmaker(bind=engine)
    >>> session = Session()
    
    >>> class User(Base):
    ...     __tablename__ = 'users'
    ...     id = sa.Column(sa.Integer, primary_key=True)
    ...     firstname = sa.Column(sa.String)
    ...     lastname = sa.Column(sa.String)
    ...     
    ...     def __repr__(self):
    ...         return "<User('%s', '%s')>" % (self.firstname, self.lastname)
    >>> Base.metadata.create_all(engine) 

Next, I populate some users :

::

    >>> for i in range(0, 200):
    ...     u = User()
    ...     u.firstname = 'Firstname%i' % i
    ...     u.lastname = 'Lastname%i' % i
    ...     session.add(u)
    >>> session.commit()

I paginate SQLAlchemy query :

::

    >>> query = session.query(User).order_by(User.firstname.desc())
    >>> p = Paginate(query, page=6, per_page=5)
    >>> p
    Page :
        Current page : 6
        First item : 26
        Last item : 30
        First page : 1
        Last Page : 40
        Previous page : 5
        Next page : 7
        Items per page : 5
        Total number of items : 200
        Total number of pages : 40
    >>> p.pages()
    [1, 2, None, 4, 5, 6, 7, 8, None, 39, 40]
    >>> [item for item in p.items]
    [<User('Firstname76', 'Lastname76')>, <User('Firstname75', 'Lastname75')>, <User('Firstname74', 'Lastname74')>, <User('Firstname73', 'Lastname73')>, <User('Firstname72', 'Lastname72')>]


Inspirations
============

* `flask-paginate <http://pythonhosted.org/Flask-paginate/>`_
* `Python pagination module <https://github.com/Signum/paginate>`_
* `webhelpers.paginate <http://docs.pylonsproject.org/projects/webhelpers/en/latest/modules/paginate.html>`_
