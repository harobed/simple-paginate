__version__ = '0.1.2'
from UserList import UserList
import textwrap

# import SQLAlchemy if available
try:
    import sqlalchemy
    import sqlalchemy.orm   # Some users report errors if this is not imported.
except:
    sqlalchemy_available = False
    sqlalchemy_version = None
else:
    sqlalchemy_available = True
    sqlalchemy_version = sqlalchemy.__version__

INCOMPATIBLE_COLLECTION_TYPE = """\
Sorry, your collection type is not supported by the paginate module. You can
provide a list, a tuple, a SQLAlchemy " "select object or a SQLAlchemy
ORM-query object."""


class Pagination(object):
    def __init__(self, page=1, per_page=10, total=0, inner_window=2, outer_window=1):
        self.page = int(page)
        self.per_page = int(per_page)
        self.inner_window = int(inner_window)
        self.outer_window = int(outer_window)
        self.total = int(total)

        if self.total > 0:
            self.first_page = 1
            self.total_pages = ((self.total - 1) // self.per_page) + 1
            self.last_page = self.first_page + self.total_pages - 1

            # Make sure that the requested page number is the range of valid pages
            if self.page > self.last_page:
                self.page = self.last_page
            elif self.page < self.first_page:
                self.page = self.first_page

            # Note: the number of items on this page can be less than
            # items_per_page if the last page is not full
            self.first_item = (self.page - 1) * per_page + 1
            self.last_item = min(self.first_item + per_page - 1, self.total)

            # Links to previous and next page
            if self.page > self.first_page:
                self.previous_page = self.page - 1
            else:
                self.previous_page = None

            if self.page < self.last_page:
                self.next_page = self.page + 1
            else:
                self.next_page = None
        else:
            self.first_page = None
            self.total_pages = 0
            self.last_page = None
            self.first_item = None
            self.last_item = None
            self.previous_page = None
            self.next_page = None

    def __repr__(self):
        return textwrap.dedent("""\
        Page :
            Current page : %(page)s
            First item : %(first_item)s
            Last item : %(last_item)s
            First page : %(first_page)s
            Last Page : %(last_page)s
            Previous page : %(previous_page)s
            Next page : %(next_page)s
            Items per page : %(per_page)s
            Total number of items : %(total)s
            Total number of pages : %(total_pages)s""" % self.__dict__)

    def pages(self):
        if self.total_pages < self.inner_window * 2 - 1:
            return range(1, self.total_pages + 1)

        pages = []
        win_from = self.page - self.inner_window
        win_to = self.page + self.inner_window
        if win_to > self.total_pages:
            win_from -= win_to - self.total_pages
            win_to = self.total_pages

        if win_from < 1:
            win_to = win_to + 1 - win_from
            win_from = 1
            if win_to > self.total_pages:
                win_to = self.total_pages

        if win_from > self.inner_window:
            pages.extend(range(1, self.outer_window + 1 + 1))
            pages.append(None)
        else:
            pages.extend(range(1, win_to + 1))

        if win_to < self.total_pages - self.inner_window + 1:
            if win_from > self.inner_window:
                pages.extend(range(win_from, win_to + 1))

            pages.append(None)
            pages.extend(range(self.total_pages - 1, self.total_pages + 1))
        elif win_from > self.inner_window:
            pages.extend(range(win_from, self.total_pages + 1))
        else:
            pages.extend(range(win_to + 1, self.total_pages + 1))

        return pages


def get_wrapper(obj, sqlalchemy_session=None):
    """
    Auto-detect the kind of object and return a list/tuple
    to access items from the collection.
    """
    # If the collection is a sequence we can use it directly
    if isinstance(obj, (list, tuple)):
        return obj

    # Is SQLAlchemy 0.4 or better available? (0.3 is not supported - sorry)
    # Note: SQLAlchemy objects aren't sliceable, so this has to be before
    # the next if-stanza
    if sqlalchemy_available and sqlalchemy_version[:3] != '0.3':
        # Is the collection a query?
        if isinstance(obj, sqlalchemy.orm.query.Query):
            return _SQLAlchemyQuery(obj)

        # Is the collection an SQLAlchemy select object?
        if (
            isinstance(obj, sqlalchemy.sql.expression.CompoundSelect) or
            isinstance(obj, sqlalchemy.sql.expression.Select)
        ):
                return _SQLAlchemySelect(obj, sqlalchemy_session)

    # If object is iterable we can use it.  (This is not true if it's
    # non-sliceable but there doesn't appear to be a way to test for that. We'd
    # have to call .__getitem__ with a slice and guess what the exception
    # means, and calling it may cause side effects.)
    required_methods = ["__iter__", "__len__", "__getitem__"]
    for meth in required_methods:
        if not hasattr(obj, meth):
            break
    else:
        return obj

    raise TypeError(INCOMPATIBLE_COLLECTION_TYPE)


class _SQLAlchemySelect(object):
    """
    Iterable that allows to get slices from an SQLAlchemy Select object
    """
    def __init__(self, obj, sqlalchemy_session=None):
        session_types = (
            sqlalchemy.orm.scoping.ScopedSession,
            sqlalchemy.orm.Session)
        if not isinstance(sqlalchemy_session, session_types):
            raise TypeError(
                "If you want to page an SQLAlchemy 'select' object then you "
                "have to provide a 'sqlalchemy_session' argument. See also: "
                "http://www.sqlalchemy.org/docs/04/session.html"
            )

        self.sqlalchemy_session = sqlalchemy_session
        self.obj = obj

    def __getitem__(self, range):
        if not isinstance(range, slice):
            raise Exception("__getitem__ without slicing not supported")
        offset = range.start
        limit = range.stop - range.start
        select = self.obj.offset(offset).limit(limit)
        return self.sqlalchemy_session.execute(select).fetchall()

    def __len__(self):
        return self.sqlalchemy_session.execute(self.obj).rowcount


class _SQLAlchemyQuery(object):
    """
    Iterable that allows to get slices from an SQLAlchemy Query object
    """
    def __init__(self, obj):
        self.obj = obj

    def __getitem__(self, range):
        if not isinstance(range, slice):
            raise Exception("__getitem__ without slicing not supported")
        return self.obj[range]

    def __len__(self):
        return self.obj.count()


class Paginate(Pagination, UserList):
    def __init__(self, collection, page=1, per_page=10, total=None, inner_window=2,
                 outer_window=1, sqlalchemy_session=None, presliced_list=False):
        self.collection = get_wrapper(collection, sqlalchemy_session)
        if total is None:
            total = len(self.collection)

        if total == 0:
            self.collection = []

        Pagination.__init__(self, page, per_page, total, inner_window, outer_window)

        self.items = self.collection
        if (not presliced_list) and (self.total > 0):
            if self.total > 0:
                try:
                    first = self.first_item - 1
                    last = self.last_item
                    self.items = list(self.collection[first:last])
                except TypeError, e:
                    if str(e) == "unhashable type":
                        # Assume this means collection is unsliceable.
                        raise TypeError(INCOMPATIBLE_COLLECTION_TYPE)
                    raise

        UserList.__init__(self, self.items)
