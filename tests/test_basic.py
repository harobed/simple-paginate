import os
import unittest
import doctest

here = os.path.dirname(__file__)


class BasicTest(unittest.TestCase):
    def test_doc(self):
        doctest.testfile(
            os.path.join('..', 'docs', 'index.rst'),
            verbose=False
        )
